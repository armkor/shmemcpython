// threaded_queue.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <Windows.h>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <chrono>
#include <conio.h>
#include <functional>

#include "safeQueue.h"

#define BUF_SIZE 4600000

using namespace std;
using namespace chrono;


void print(string x) {
	static mutex mutex_;
	unique_lock<mutex> locker(mutex_);
	cout << x << "\n";
}

void writeShm(Queue<char*> &q, int frameNum) {

	string prefix = to_string(frameNum);
	string readEventName = "READ_" + prefix;
	string writeEventName = "WRITE_" + prefix;
	string sharedName = "SHARED_" + prefix;

	HANDLE readEvent = CreateEvent(NULL, false, false, wstring(readEventName.begin(), readEventName.end()).c_str());
	HANDLE writeEvent = CreateEvent(NULL, false, false, wstring(writeEventName.begin(), writeEventName.end()).c_str());

	HANDLE hMapFile = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, BUF_SIZE, wstring(sharedName.begin(), sharedName.end()).c_str());

	LPCTSTR pBuf = (LPTSTR)MapViewOfFile(hMapFile, FILE_MAP_ALL_ACCESS, 0, 0, BUF_SIZE);

	print(string("Tread " + to_string(frameNum) + " is ready" ));

	do
	{
		auto item = q.pop();
		CopyMemory((PVOID)pBuf, item, BUF_SIZE * sizeof(char));
		delete[] item;

		ostringstream tmp;
		//tmp << "\t\t Just poped item:" << +item << " -->";
		//print(tmp.str());

		if (!SetEvent(writeEvent) || WaitForSingleObject(readEvent, INFINITE) == WAIT_FAILED) {
			printf("1:%u\n", GetLastError());
			break;
		}
	} while (true);

}

static int numOfWorkers = 2;

int main()
{

	char currentValue = 0;

	vector<thread> writers;
	vector<Queue<char*>*> queues;

	for (int i = 0; i < numOfWorkers; i++) 
	{
		Queue<char*>* queue = new Queue<char*>();
		queues.push_back(queue);

		thread writer(std::bind(writeShm, ref(*queue), i));
		writers.push_back(move(writer));
	}


	cin.get();
	int needPrint = 0;
	while (true)
	{
		this_thread::sleep_for(chrono::milliseconds(5));
		
		ostringstream tmp;
		tmp << "Writer --> " << +currentValue << "::" << duration_cast<milliseconds>(
			system_clock::now().time_since_epoch()).count() << endl;
		
		char* hh = new char[BUF_SIZE];
		memset(hh, (char)currentValue, BUF_SIZE);
		if (needPrint % 200 == 0) print(tmp.str());
		if (needPrint % 201 == 0) print(tmp.str());
		queues[abs(currentValue % numOfWorkers)]->push(hh);
		currentValue++;
		needPrint++;
	}

	for (auto& writer : writers) {
		writer.join();
	}

}