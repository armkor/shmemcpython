import ctypes as C
import logging
import numpy as np
import time

from mmap import mmap
from mmap import ACCESS_WRITE
from timeit import default_timer as timer
from time import sleep
from weakref import finalize
from win32api import CloseHandle
from win32event import CreateEvent, ResetEvent, SetEvent, WaitForSingleObject, OpenEvent

mm = mmap(-1, 4600000, "SHARED_0", ACCESS_WRITE)

read = OpenEvent(2, 0, "READ_0")
write = OpenEvent(0x00100000, 0, "WRITE_0")
i = 0
while 1 < 200:
	if WaitForSingleObject(write, 0xFFFFFFFF) == 0xFFFFFFFF:
		print("foo")
		break
	mm.seek(0)
	if i % 100 == 0
		print("Read data from memory:", mm[0], ":::", time.time());
	data = mm.read()
	#print('Read', len(data), 'bytes')
	i = i + 1
	SetEvent(read)

CloseHandle(write)
CloseHandle(read)

